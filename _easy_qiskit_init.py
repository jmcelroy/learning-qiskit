#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 20 07:02:46 2022

@author: jmcelroy
"""

# outside libraries
import os, copy, sys, math

from math import pi

import matplotlib.pyplot as plt
import numpy as np

from sklearn import metrics

import warnings
warnings.filterwarnings('ignore', category=FutureWarning)

# qiskit
import qiskit

# environmental parameters
os.environ['QISKIT_PARALLEL'] = 'true'
os.environ['QISKIT_FORCE_THREADS'] = 'true'
os.environ['QISKIT_IBMQ_PROVIDER_LOG_LEVEL'] = 'info'
os.environ['QISKIT_IBMQ_PROVIDER_LOG_FILE'] = 'new_qiskit_log'

from qiskit import (Aer, IBMQ, QuantumRegister, ClassicalRegister,
                    transpile, schedule as build_schedule)

# .providers
from qiskit.providers.aer import noise
from qiskit.providers.aer import AerSimulator, QasmSimulator

from qiskit_ibm_provider import IBMProvider, least_busy

# .visualization
from qiskit.visualization import plot_state_city
from qiskit.visualization.bloch import Bloch

# .tools
from qiskit.tools.visualization import plot_histogram as ph, plot_bloch_multivector
from qiskit.tools.monitor import job_monitor

# .circuit
from qiskit.circuit import QuantumCircuit, Parameter, ParameterVector
from qiskit.circuit.library.phase_oracle import PhaseOracle
from qiskit.circuit.library import (ZZFeatureMap,
                                    TwoLocal,
                                    NLocal,
                                    EfficientSU2,
                                    RealAmplitudes)
from qiskit.circuit.library.standard_gates import HGate, XGate, YGate, ZGate

# .utils
from qiskit.utils import QuantumInstance
from qiskit.utils.mitigation import (complete_meas_cal,
                                     tensored_meas_cal,
                                     CompleteMeasFitter,
                                     TensoredMeasFitter)

# .transpiler
from qiskit.transpiler import PassManager
from qiskit.transpiler.passes.calibration import RZXCalibrationBuilderNoEcho

# _nature
from qiskit_nature.drivers import UnitsType, Molecule
from qiskit_nature.second_q.drivers import *

from qiskit_nature.problems.second_quantization import ElectronicStructureProblem
from qiskit_nature.converters.second_quantization import QubitConverter
from qiskit_nature.transformers.second_quantization.electronic import FreezeCoreTransformer
from qiskit_nature.mappers.second_quantization import ParityMapper, JordanWignerMapper
from qiskit_nature.algorithms import GroundStateEigensolver
from qiskit_nature.runtime import VQEClient
from qiskit_nature.operators.second_quantization import FermionicOp
from qiskit_nature.converters.second_quantization.qubit_converter import *

# ML
import qiskit_machine_learning
from qiskit_machine_learning.datasets import ad_hoc_data
from qiskit_machine_learning.algorithms import QSVC
from qiskit_machine_learning.kernels import QuantumKernel

# .algorithms
from qiskit.algorithms import NumPyMinimumEigensolver, VQE, Shor
from qiskit.algorithms.optimizers import SPSA, GradientDescent, QNSPSA

# .opflow
from qiskit.opflow import (X, Y, Z, I, CircuitSampler, StateFn,
                           Zero, One, PauliTrotterEvolution,
                           Suzuki, CircuitOp, CircuitStateFn)
from qiskit.opflow.expectations import (PauliExpectation,
                                        MatrixExpectation,
                                        AerPauliExpectation)
from qiskit.opflow.gradients import Gradient, NaturalGradient, Hessian

# other
from qiskit.exceptions import MissingOptionalLibraryError as mole

import qiskit.quantum_info as qi
from qiskit.quantum_info import Statevector, partial_trace, DensityMatrix
from qiskit.quantum_info.operators import Operator

from qiskit_dynamics import Solver
from qiskit_dynamics.pulse import InstructionToSignals
